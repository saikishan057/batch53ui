function addition (num1,num2){//parameters
        return(num1+num2);
}
addition(4,5);//arguments

function square (a){
    return(a*a);
}
var value=square(10);
console.log(value);

function cube (b){
    return(b*b*b);
}
var value=cube(8);
console.log(value);

console.log(Math.max(3,6));
console.log(Math.min(50,10));
console.log(Math.floor(4.3));
console.log(Math.round(5.8));

const a=[30,59,67,78,87]
let max=0;
a.forEach((element)=>{
    if(element>max){
        max=element;
    }
})
console.log(max);

const arr=[12,14,15,16,17]
let sumelem=1
arr.forEach((element)=>{
    sumelem=sumelem*element
})
console.log(sumelem);

let person ={
    name1 : 'mahesh',
    batch : 53,
    present :true,
    skills :[`java`,`html`,`sql`]
}
console.log(`Name of person is ${person.name1} and he is from batch ${person.batch}`);